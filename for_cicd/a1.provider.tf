terraform {
  required_providers {
    google = {
        source = "hashicorp/google"
    }
    random = {
      source  = "hashicorp/random"
     # version = "~>3.0"
    }
   
}
}
provider "google" {
    ##project name you will get it from the json file
  project = var.project
  region = "us-central-1"
  zone = "us-central-1a"
  credentials = "./key.json"
}