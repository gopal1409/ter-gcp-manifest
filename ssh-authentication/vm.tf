##if i execute this resource block it will always create a single instance
resource "google_compute_instance" "default" {
  
  name         = "${local.resource_name_prefix}-vm"
  machine_type = var.vm_size
  zone         = var.zone
##firewall taga
  tags = ["allow-icmp", "custom-web"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size = 15
    }
  }


  network_interface {
    network = google_compute_network.custom-vpc-tf.id
    subnetwork = google_compute_subnetwork.asia-southeast1-subnet.id

    access_config {
        ##api.label.specification
     #nat_ip = google_compute_address.publicip.address
    }
  }
##terra
  metadata = {
    "ssh-keys" = <<EOT 
      gopal:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQ+j+hMCQgsVnfGZXB3qUPkKWLPRKEQ+VE/gsuZAH0v2QoOi8Q7oO8+i+JSfqovMK709lD9NUbOYiUGU3xvXudgf/6vLviVLPS2eM6Z7dG3xQM88e3PbOqWmdaDo9eQI0BOjvr2+FNH0E+2mO9YqbdABml2F83wrAMmmOZJHLWKfxb75oph2uFO/3eluhmMm1Zbg8oJZVD0MmzMWQlaDPUdjC8ulgCXgF+hMI8eFEWVadTWpr9PVvQBwZvyuS5JT5AFzTSKJE3wAqERs9WGphW9ak5mW/WjOoH5V+bebRLPdGUYjuSaYK+9ZcXW6TLub1srBkT7IoeykfozT0hwRFx gopal
    EOT
  }

  metadata_startup_script = file("${path.module}/tomcat.sh")
  
}
