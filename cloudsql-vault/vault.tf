variable "secret" {
  type = object({
    name       = string
    vault_path = string
  })
  description = "A vault secret"
}

# get the secret 
data "vault_generic_secret" "this" {
  path     = var.secret.vault_path
}

# create a SM secret for the vault secret  
resource "google_secret_manager_secret" "this1" {
  secret_id = "my-sm-${var.secret.name}"
  replication {
    auto {}
  }
}

# create a SM secret version for the vault secret 'value'  
resource "google_secret_manager_secret_version" "this2" {
  secret      = google_secret_manager_secret.this1.id
  secret_data = data.vault_generic_secret.this.data["value"]
}