#wee will create an public ip for our instance
resource "google_compute_address" "publicip" {
  name = "vm-publicip" 
  project = var.project
  region = var.region_test
  #we use depend on
  depends_on = [ google_compute_firewall.ssh-firewall ]
}

#natgw is an fully managed gcp service. 
#then only my db instance will get internet connection
#depend_on = natgew