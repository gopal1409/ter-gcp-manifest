resource "google_sql_database_instance" "main" {
  name             = "${local.resource_name_prefix}cloud-sql"
  database_version = "MYSQL_8_0"
  region           = "us-central1"

  settings {
    # Second-generation instance tiers are based on the machine
    # type. See argument reference below.
    tier = "db-f1-micro"
  }
  deletion_protection = false 

}

resource "google_sql_database" "name" {
  name = "${local.resource_name_prefix}cloud-db"
  instance = google_sql_database_instance.main.name
}

resource "google_sql_user" "myuser" {
  name = "tflearner"
  instance = google_sql_database_instance.main.name 
   #password = "admin12345"
  password = data.vault_generic_secret.this.data["value"]
  #secret

}