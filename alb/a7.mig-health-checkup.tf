resource "google_compute_health_check" "tcp-health-check" {
  name        = "${local.resource_name_prefix}-mig-health-checkup"
  description = "Health check via http"

  timeout_sec         = 5
  check_interval_sec  = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10

  http_health_check {
port = "80"
request_path = "/"
  }
}