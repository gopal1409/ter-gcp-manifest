
resource "google_compute_instance_template" "gcp-template" {
  name        = "${local.resource_name_prefix}-mig-template"
  description = "This template is used to create app server instances."
 #firewall tags
  tags = ["allow-icmp", "allow-web"]

  labels = {
    environment = "dev"
  }

  instance_description = "description assigned to instances"
  machine_type         = var.vm_size
  can_ip_forward       = false

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE" #down the line all the vm are running in some physical hardware. 
  }

  // Create a new boot disk from an image
  disk {
    source_image      = "ubuntu-os-cloud/ubuntu-2004-lts"
    auto_delete       = true
    boot              = true
    // backup the disk every day
    #resource_policies = [google_compute_resource_policy.daily_backup.id]
  }

  // Use an existing disk resource
  

  network_interface {
    network = "default"
    access_config {
      
    }
  }

  metadata_startup_script  = file("${path.module}/app/app.sh")
}

