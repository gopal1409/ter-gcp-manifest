resource "google_compute_health_check" "lb-health-check" {
  name        = "${local.resource_name_prefix}-lb-health-checkup"
  description = "Health check via http"

  timeout_sec         = 5
  check_interval_sec  = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10

  http_health_check {
port = "80"
port_specification = "USE_FIXED_PORT" #in which port the alb is going to do the healtch checkup
proxy_header = "NONE" #we are disabling the proxy server
request_path = "/" #/app
  }
}