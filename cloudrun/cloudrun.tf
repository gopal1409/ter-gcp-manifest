resource "google_cloud_run_service" "cloud-run-tf" {
  name     = "${local.resource_name_prefix}cloud-run-tf"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}