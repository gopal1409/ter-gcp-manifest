#this will crete secret manager inside gcp
resource "google_secret_manager_secret" "secret1" {
  secret_id = "my-sm-${var.secret.name}"
  replication {
    auto {}
  }
}
#inside the secret manager we need to call the secret from vault 
resource "google_secret_manager_secret_version" "secret-version" {
  secret = google_secret_manager_secret.secret1.id
  secret_data = data.vault_generic_secret.this.data["value"]
}