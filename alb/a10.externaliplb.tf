resource "google_compute_global_address" "external_ip_lb" {
  name = "${local.resource_name_prefix}-externaliplb"
  ip_version = "IPV4"
}