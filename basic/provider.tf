terraform {
  required_providers {
    google = {
        source = "hashicorp/google"
    }
   
}

provider "google" {
    ##project name you will get it from the json file
  project = ""
  region = "us-central-1"
  zone = "us-central-1a"
  credentials = "./key.json"
}