###backedn service

resource "google_compute_backend_service" "default" {
  name          = "${local.resource_name_prefix}-backend-service"
  connection_draining_timeout_sec = 0
  health_checks = [google_compute_health_check.lb-health-check.id]
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_name = "http" #or https 
  #suppose i am running an flask app 5000 loadbalancer also take request in 5000 port
  protocol = "HTTP"
session_affinity = "NONE" #sticky session
timeout_sec = 30
backend {
  group = google_compute_instance_group_manager.appserver.instance_group
  balancing_mode = "UTILIZATION"
  capacity_scaler = 1.0
}
}

resource "google_compute_url_map" "default" {
  name = "web-mapp-http"
  default_service = google_compute_backend_service.default.id 
}

resource "google_compute_target_http_proxy" "default" {
  name = "http-lb-proxy"
  url_map = google_compute_url_map.default.id
}

resource "google_compute_global_forwarding_rule" "default" {
  name = "http-content-rule"
  ip_protocol = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_range = "80-80"
  target = google_compute_target_http_proxy.default.id 
  ip_address = google_compute_global_address.external_ip_lb.id
}