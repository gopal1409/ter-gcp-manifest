resource "google_compute_firewall" "allow-icmp" {
  name    = "allow-icmp"
  network = google_compute_network.custom-vpc-tf.name

  allow {
    protocol = "icmp"
  }

source_ranges = [ "0.0.0.0/0" ]
  priority = 500

  source_tags = ["custom-icmp"]
}
#we are adding a new rule
resource "google_compute_firewall" "allow-web" {
  name    = "allow-web"
  network = google_compute_network.custom-vpc-tf.name

  allow {
    protocol = "tcp"
    ports = [ "22","80","8080" ]
  }

source_ranges = [ "0.0.0.0/0" ]


  source_tags = ["custom-web"]
}
