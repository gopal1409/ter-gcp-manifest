resource "google_compute_autoscaler" "default" {
  #provider = google-beta

  name   = "${local.resource_name_prefix}-mig-autoscaler"
  zone   = var.zone
  target = google_compute_instance_group_manager.appserver.id

  autoscaling_policy {
    max_replicas    = 4
    min_replicas    = 2
    cooldown_period = 60 #cool down period
    #the time taken to spin up the instance before it will be part of your mig
    #you know your application take 2 min before it get fully deployment inside your vm
  }
}
