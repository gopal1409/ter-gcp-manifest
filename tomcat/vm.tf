##if i execute this resource block it will always create a single instance
resource "google_compute_instance" "default" {
  
  name         = "${local.resource_name_prefix}-vm"
  machine_type = var.vm_size
  zone         = var.zone
##firewall taga
  tags = ["allow-icmp", "custom-web"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size = 15
    }
  }


  network_interface {
    network = google_compute_network.custom-vpc-tf.id
    subnetwork = google_compute_subnetwork.asia-southeast1-subnet.id

    access_config {
        ##api.label.specification
     #nat_ip = google_compute_address.publicip.address
    }
  }
##terra


  metadata_startup_script = file("${path.module}/tomcat.sh")
  
}
