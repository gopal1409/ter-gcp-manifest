

resource "google_compute_instance_group_manager" "appserver" {
  name = "${local.resource_name_prefix}-mig"

  base_instance_name = "app"
  zone               = var.zone
#we need to attach the instance template
  version {
    instance_template  = google_compute_instance_template.gcp-template.self_link_unique
  }

  

  

  named_port {
    name = "customhttp"
    port = 80
  }

  auto_healing_policies {
    health_check      = google_compute_health_check.tcp-health-check.id
    initial_delay_sec = 300
  }
}