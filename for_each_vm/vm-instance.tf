##if i execute this resource block it will always create a single instance
resource "google_compute_instance" "default" {
  for_each = local.virtual_machines
  name         = each.key 
  #sap-hr-0
  machine_type = each.value.vm_size
  zone         = each.value.zone
##firewall taga
  tags = ["externalssh", "webserver"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }


  network_interface {
    network = "default"

    access_config {
        ##api.label.specification
     #nat_ip = google_compute_address.publicip.address
    }
  }
##terra

  metadata_startup_script = file("${path.module}/app.sh")
  
}
