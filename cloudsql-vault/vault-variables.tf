variable "secret" {
  type = object({
    name = string
    vault_path = string
  })
  description = "a vault secret engine and path"
}

