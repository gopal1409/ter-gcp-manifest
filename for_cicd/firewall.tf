resource "google_compute_firewall" "ssh-firewall" {
  name    = "gritfy-firewall-ssh-external-${random_pet.pet.id}"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["22","8080"]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = [ "externalssh" ]
}

#inside your terraform project folder all the label name against your resource will be always unqie 
resource "google_compute_firewall" "http-firewall" {
  name    = "gritfy-firewall-http-external-${random_pet.pet.id}"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["80","443"]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = [ "webserver" ]
}
