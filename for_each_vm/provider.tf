terraform {
  required_providers {
    google = {
        source = "hashicorp/google"
    }
   
}
}
provider "google" {
    ##project name you will get it from the json file
  project = var.project
  region = var.region_test
  zone = var.zone
  credentials = "./key.json"
}

