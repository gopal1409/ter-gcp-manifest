resource "google_compute_network" "custom-vpc-tf" {
  name                    = "${local.resource_name_prefix}-vpc"
  #it will automatically create subnet in all the region
  #but we dont need all the region will be enabled. 
  #it will be always a security risk
  auto_create_subnetworks = false
  #inside your vpc packet transimission unit
  #mtu is optional
  mtu                     = 1460
  
}
#inside the vpc we will have a subnet
#complaince for your application. 
#we will create subnet i have business in asia-southeast

resource "google_compute_subnetwork" "asia-southeast1-subnet" {
    #it is going to create a subnet
    #the subnet need to be part of vpc so we meed to map the subnet with vpc
  name = "${local.resource_name_prefix}-subnet-asia"
  #this subnet need to be part of VPC
  #whenenver you want to map the resource
  #resourceapiname.ref.id
  network = google_compute_network.custom-vpc-tf.id
  
  #network = "projects/gopaldas1705-cts/global/networks/sap-hr-vpc"
  region = "asia-southeast1"
  #this is the private ip range
  ip_cidr_range = "10.1.0.0/24"
}

