variable "email" {
  type = string 
  default = "gopal1409@gmail.com"
}

variable "privatekeypath" {
  type = string 
  default = "~/.ssh/id_rsa"
}

variable "publickeypath" {
  type = string 
  default = "~/.ssh/id_rsa.pub"
}

variable "names" {
  type = set(string)
  default = [ "dev", "test","staging","prod" ]
}

variable "business_division" {
  #happy
  type = string
  default = "sap"
}

variable "environment" {
  #sad
  type = string
  default = "hr"
}
variable "pass" {
  type = string
  sensitive = true 
  default = "pass123"
  #base64 encoded format inside your tfstate file
}
#i want to merge both the expressing with one identity 
variable "vm_size" {
  type = string
  default = "n1-standard-1"
}

variable "zone" {
  type = list[string] 
  default = ["asia-southeast1-a","asia-southeast1-b"]
}