#this will create a server for my database
resource "google_sql_database_instance" "main" {
  name = "mysqldb"
  database_version = "MYSQL_8_0"
  region = var.region_test

  settings {
    tier = "db-f1-micro"
  }
  deletion_protection = false
}

#lets create the database 
resource "google_sql_database" "name" {
  name = "cloud-db"
  instance = google_sql_database_instance.main.name
}

#inside the db i want to create an user and password
resource "google_sql_user" "myuser" {
  name = "tflearner"
  instance = google_sql_database_instance.main.name 
  password = data.vault_generic_secret.this.data["value"]
}