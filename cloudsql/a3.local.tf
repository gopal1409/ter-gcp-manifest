locals {
  resource_name_prefix = "${var.business_division}-${var.environment}"
  virtual_machines = {
    "vm1" = {vm_size = "e2-small",zone = "us-central1-a"},
    "vm2" = {vm_size = "e2-medium",zone = "us-central1-b"},
    "vm3" = {vm_size = "e2-micro",zone = "us-central1-c"}
  }
  #resource_name_prefix = sap-hr
}
#local.resource_name_prefix
#local.virtual_machines
#contains multiple key and value
