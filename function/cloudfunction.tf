resource "google_cloudfunctions2_function" "function" {
    depends_on = [ google_storage_bucket.bucket_from_username ]
  name = "function-v2"
  location = "us-central1"
  description = "a new function"

  build_config {
    runtime = "nodejs20"
    entry_point = "helloHttp"  # Set the entry point 
    source {
      storage_source {
        bucket = google_storage_bucket.bucket_from_username.name
        object = google_storage_bucket_object.source_code.name
      }
    }
  }

  service_config {
    min_instance_count = 1
    max_instance_count  = 3
    available_memory    = "256M"
    timeout_seconds     = 60
    all_traffic_on_latest_revision = true
  }
}

output "function_uri" { 
  value = google_cloudfunctions2_function.function.service_config[0].uri
}

resource "google_cloud_run_service_iam_binding" "default" {
  location = google_cloudfunctions2_function.function.location
  service = google_cloudfunctions2_function.function.name
  role = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}