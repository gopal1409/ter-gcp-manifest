resource "google_storage_bucket" "bucket_from_username" {
  #the bucket name i am calling it through varaibles
  name = "${local.resource_name_prefix}-${random_pet.pet.id}"
  location = "us-central1"
}

resource "google_storage_bucket_object" "source_code" {
  name = "object"
  bucket = google_storage_bucket.bucket_from_username.name
  source = "index.zip"
}

